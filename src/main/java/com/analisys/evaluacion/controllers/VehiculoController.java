package com.analisys.evaluacion.controllers;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.analisys.evaluacion.repository.IVehiculoRepository;
import com.analisys.evaluacion.models.entitys.Vehiculo;


@RestController
@RequestMapping("/api")
public class VehiculoController {

	@Autowired
	IVehiculoRepository vehiculoRepository;

	@GetMapping("/vehiculos")
	public List<Vehiculo> all() {
		return vehiculoRepository.findAll();
	}

	@GetMapping("/vehiculos/{id}")
	public Vehiculo show(@PathVariable Integer id) {
		return vehiculoRepository.findById(id).get();
	}

	@PostMapping("/vehiculos")
	@ResponseStatus(HttpStatus.CREATED) // Deberia entregar codigo 201
	public Vehiculo create(@RequestBody Vehiculo entity) {
		return vehiculoRepository.save(entity);
	}

	@PutMapping("/vehiculos/{id}")
	public Vehiculo update(@RequestBody Vehiculo entity, @PathVariable Integer id) {
		
		Optional<Vehiculo> modificar = vehiculoRepository.findById(id);

		if (modificar.isPresent()) {
			System.out.println("id encontrado!"+id);
			modificar.get().setId(id);
			modificar.get().setPatente(entity.getPatente());
			modificar.get().setMarca(entity.getMarca());
			modificar.get().setModelo(entity.getModelo());
			modificar.get().setPeso(entity.getPeso());
			modificar.get().setColor(entity.getColor());
			return vehiculoRepository.save(modificar.get());
		} else {
			System.out.println("No existe el id de la URL");
		}

	
		return null;

	}

	@DeleteMapping("/vehiculos/{id}")
	public void delete(@PathVariable Integer id) {
		vehiculoRepository.deleteById(id);
	}

}
