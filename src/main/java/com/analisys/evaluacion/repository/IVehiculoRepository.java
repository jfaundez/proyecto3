package com.analisys.evaluacion.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.analisys.evaluacion.models.entitys.Vehiculo;

public interface IVehiculoRepository extends JpaRepository<Vehiculo,Integer> {

}
