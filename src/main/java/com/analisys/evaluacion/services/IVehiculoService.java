package com.analisys.evaluacion.services;

import java.util.List;

import com.analisys.evaluacion.models.entitys.Vehiculo;

public interface IVehiculoService {

	public List<Vehiculo> all();

	public Vehiculo show(Integer id);

	public Vehiculo create(Vehiculo entity);

	public void delete(Integer id);

}
