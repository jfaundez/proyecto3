package com.analisys.evaluacion.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.analisys.evaluacion.repository.IVehiculoRepository;

import com.analisys.evaluacion.models.entitys.Vehiculo;

@Service
public class VehiculoServiceImpl implements IVehiculoService {

	@Autowired
	IVehiculoRepository vehiculoRepository;

	@Override
	public List<Vehiculo> all() {
		return vehiculoRepository.findAll();
	}

	@Override
	public Vehiculo show(Integer id) {
		return vehiculoRepository.findById(id).get();
	}

	@Override
	public Vehiculo create(Vehiculo entity) {
		return vehiculoRepository.save(entity);
	}

	@Override
	public void delete(Integer id) {
		vehiculoRepository.deleteById(id);
	}

}
