package com.analisys.evaluacion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppEvaluacion3Application {

	public static void main(String[] args) {
		SpringApplication.run(AppEvaluacion3Application.class, args);
	}

}
