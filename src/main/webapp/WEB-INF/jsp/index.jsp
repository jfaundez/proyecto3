<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>ciisa.cl</title>
</head>
<style>
body {
	font-family: "Arial";
	display:flex;
	justify-content:center;
	flex-direction:column;
	place-items:center;
}

section{
	width:70%;
}


table {
	border-collapse: collapse;
	border: 1px solid #ddd;
}

thead>th>td {
	font-weight: bold;
}

td {
	padding: 10px;
	border-bottom: 1px solid #ddd;
}
</style>
<body>

	

	<section>
		<h1>CIISA</h1>
		<h2>Jean Carlos Faúndez Sáez</h2>
		<h3>Ingeniería en Informática</h3>
	</section>
	
	<section>
	<table>
		<thead>
			<tr>
				<th>API</th>
				<th>Verbo HTTP</th>
				<th>Descripcion</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><a href="/api/vehiculos">api/vehiculos</a></td>
				<td>GET</td>
				<td>Metodo encargado de realizar consulta a traves de la URL y
					devolviendo una respuesta null,exception o datos.</td>
			</tr>
			
				<tr>
				<td><a href="/api/vehiculos">api/vehiculos/{id}</a></td>
				<td>GET</td>
				<td>Metodo encargado de realizar consulta a traves de la URL y pasando un parametro. y
					devolviendo una respuesta null,exception o datos</td>
			</tr>

			<tr>
				<td><a href="/api/vehiculos">api/vehiculos</a></td>
				<td>POST</td>
				<td>Metodo que permite realizar un insert en una base de datos,
					a traves de una petición http, la cual se envia los datos desde el
					cliente al servidor por medio de "consulta de cabecera (HEADERS)"
					enviando datos al servidor.</td>
			</tr>
			<tr>
				<td><a href="/api/vehiculos">api/vehiculos</a></td>
				<td>PUT</td>
				<td>Metodo que permite relizar una modificacion en una base de
					datos, a traves de una peticion http, en la cual se envian los
					datos desde el cliente al servidor por medio de "conmsulta de
					cabecera (HEADER) y la URL la cual tendra el identificador (ID) del
					objetos que se requiere modificar.</td>
			</tr>
			<tr>
				<td><a href="/api/vehiculos">api/vehiculos</a></td>
				<td>DELETE</td>
				<td>Metodo encargado de realizar consulta a traves de la URL
					via parametro y devolviendo una respuesta null,exception o datos.</td>
			</tr>
		</tbody>

	</table>
	
	</section>
	
	
	

</body>
</html>